copy (select horestacao as station_id, to_char(hordatahora, 'YYYY-MM-DD HH24:MI:SS') as datetime, horleitura as solarrad
from horaria
where horestacao in (select estcodigo from telemetria.estacao where estorgao = 1 and esttipo = 'M')
and horsensor=6
and horqualidade in (0, 1)
and hordatahora >= :start
and hordatahora < :end
order by hordatahora) to STDOUT delimiter ',' csv header;
