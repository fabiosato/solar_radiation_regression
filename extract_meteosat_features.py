#!/usr/bin/python
import sys
import csv
from calendar import monthrange
import math
import numpy as np
from osgeo import gdal, osr
from netCDF4 import Dataset
import traceback
import os.path

#gdal.UseExceptions()
gdal.PushErrorHandler('CPLQuietErrorHandler')

# configuration
# satellite root data directory
eumetsat_l2_dir = '/simepar/satelite/eumetsat/met-10/HR-SEVIRI/L2'

# subfolders of each channel
channels = ['IR_016', 'IR_039', 'IR_087', 'IR_097', 'IR_108', 'IR_120', 'IR_134', 'VIS006', 'VIS008', 'WV_062', 'WV_073']
#channels = ['IR_087', 'VIS008']

# years and months to be processed
years = range(2015, 2016)
months = range(8, 13)

# output filename
output_filename = 'eumetsat_features.csv'

# dictionary that will hold the stations metadata
stations = {}

# load weather stations metadata (id, name, lat, lon, elevation) from csv file
with open('data/features/station/stations.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        record = {}
        try:
            record['elevation'] = float(row['elevation'])
        except Exception:
            record['elevation'] = None
        record['lon'] = float(row['lon'])
        record['lat'] = float(row['lat'])
        record['name'] = row['name']
        id = row['id']
        stations[id] = record

# dictionary that will hold the extracted features
features = {}
means = {}
stds = {}

for station_id in stations:
    features[station_id] = {}
    means[station_id] = {}
    stds[station_id] = {}

# loop over station ids, years, months, days and hours extracting satellite features
for year in years:
    for month in months:
        if (year == 2013) and (month < 7):
            continue
        (first_weekday, last_day) = monthrange(year, month)
        # loop over the days of the month
        days = range(0, last_day+1)
        for day in days:
            print("day: %02d" % day)
            # loop over the hours of the day
            for hour in range(0, 24):
                timestamp = "%04d-%02d-%02d %02d:00" % (year, month, day, hour)
                for ch in channels:
                    path = '%s/%s/%04d/%02d/%02d/%04d%02d%02d%02d00_MET_%s_L2.nc' % (eumetsat_l2_dir, ch, year, month, day, year, month, day, hour, ch)
                    if os.path.isfile(path):
                        print("Processing %s" % path)
                        # print '%s %s %s %s %s' % (year, month, day, hour, minute)
                        try:
                            ds = gdal.Open(path, gdal.GA_ReadOnly)
                            gt = ds.GetGeoTransform()
                            band = ds.GetRasterBand(1)
                            #data = band.ReadAsArray(0, 0, ds.RasterXSize, ds.RasterYSize)
                            nc = Dataset(path)
                            try:
                                data = np.array(nc.variables[ch])
                            except:
                                data = np.array(nc.variables['ir_108'])
                            if len(data) == 1:
                                data = data[0]
                            xres = gt[1]
                            yres = gt[5]
                            xmin = gt[0] + (xres * 0.5)
                            xmax = gt[0] + (xres * ds.RasterXSize) - (xres * 0.5)
                            ymin = gt[3] + (yres * ds.RasterYSize) + (yres * 0.5)
                            ymax = gt[3] - (yres * 0.5)

                            for station_id in stations:
                                try:
                                    if timestamp not in features[station_id]:
                                        features[station_id][timestamp] = {}
                                    if timestamp not in means[station_id]:
                                        means[station_id][timestamp] = {}
                                    if timestamp not in stds[station_id]:
                                        stds[station_id][timestamp] = {}
                                        
                                    s = stations[station_id]
                                    x = int(round((s['lon'] - xmin) / xres, 0))
                                    y = int(round((s['lat'] - ymax) / yres, 0))
                                    # print data[x][y]
                                    features[station_id][timestamp][ch] = data[y][x]
                                    
                                    pixels = data[y-1:y+1, x-1:x+1]
                                    means[station_id][timestamp][ch] = np.mean(pixels)
                                    stds[station_id][timestamp][ch] = np.std(pixels)

                                    #print("%s" % stations[station_id]['name'])
                                    #print("pixel=%f\tmean=%f\tstd=%f" % (features[station_id][timestamp][ch], means[station_id][timestamp][ch], stds[station_id][timestamp][ch]))
                                except:
                                    print("Unexpected error:", sys.exc_info()[0])
                                    traceback.print_exc()
                        except:
                            print("Error while opening the file")
                            continue

            output_filename = "data/features/satellite/satellite_features_%04d_%02d_%02d.csv" % (year, month, day)
            print("Writing %s" % output_filename)
            output = open(output_filename, "w")

            output.write("datetime,")
            output.write("station_id,")
            
            for idx, ch in enumerate(sorted(channels)):
                output.write(ch)
                if idx < len(channels) - 1:
                    output.write(",")
            
            for idx, ch in enumerate(sorted(channels)):
                output.write('%sM' % ch)
                if idx < len(channels) - 1:
                    output.write(",")
                    
            for idx, ch in enumerate(sorted(channels)):
                output.write('%sS' % ch)
                if idx < len(channels) - 1:
                    output.write(",")
                    
            output.write("\n")

            # write features
            #for day in days:
            for hour in range(0, 24):
                timestamp = "%04d-%02d-%02d %02d:00" % (year, month, day, hour)
                for s in features.keys():
                    if timestamp in features[s]:
                        output.write(timestamp)
                        output.write(",")
                        output.write(s)
                        output.write(",")
                        for idx, ch in enumerate(sorted(channels)):  
                            if ch in features[s][timestamp]:
                                output.write(str(features[s][timestamp][ch]))
                                output.write(",")
                        for idx, ch in enumerate(sorted(channels)):  
                            if ch in features[s][timestamp]:
                                output.write(str(means[s][timestamp][ch]))
                                output.write(",")
                        for idx, ch in enumerate(sorted(channels)):  
                            if ch in features[s][timestamp]:
                                output.write(str(stds[s][timestamp][ch]))
                                if idx < len(channels) - 1:
                                    output.write(",")
                        output.write("\n")
            output.close()
