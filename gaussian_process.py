import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn import gaussian_process

import utils
import regression

import sys
import logging

data = utils.load_data()

# split into feature inputs and target
X = data.loc[:, data.columns[1]:data.columns[len(data.columns)-2]]
y = data.loc[:, 'radiation']

# scale features data
X = preprocessing.normalize(X)

# prepare train and test databases
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

clf = gaussian_process.GaussianProcess(theta0=0.01, thetaL=0.0001, thetaU=0.1, verbose=True)
clf.fit(X, y)


print("Training")
clf.fit(X_train, y_train)

y_pred_train = clf.predict(X_train)
print("Training dataset")
regression.report(y_train, y_pred_train)
print("")

y_pred_test = clf.predict(X_test)
print("Test dataset")
regression.report(y_test, y_pred_test)