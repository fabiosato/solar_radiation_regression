import pandas as pd
import math
from sklearn.ensemble import AdaBoostRegressor
from sklearn.cross_validation import train_test_split

import regression
import utils

data = utils.load_data()

# split into feature inputs and target
X = data.loc[:, data.columns[1]:data.columns[len(data.columns)-2]]
y = data.loc[:, 'radiation']

# prepare train and test databases
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

estimators = 1000
clf = AdaBoostRegressor(n_estimators=estimators, loss='square', learning_rate=0.01)

print("Train model")
clf.fit(X_train, y_train)

y_pred_train = clf.predict(X_train)
print("Training dataset")
regression.report(y_train, y_pred_train)
print("")

y_pred_test = clf.predict(X_test)
print("Test dataset")
regression.report(y_test, y_pred_test)
print("")

print("Entire dataset")
y_pred = clf.predict(X)
regression.report(y, y_pred)
