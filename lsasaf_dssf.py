# -*- coding: utf-8 -*-
import bz2
import tempfile
import os
import h5py
from datetime import datetime

def extract_features(filepath, indexes, ids):
    """ 
    Extract DSSF values from a file given a list of indexes
    of the poitns of interest.

    This function decompress the original data to a new file because h5py can't
    handle compressed files and it can be given only a file path string, not file objects
    or data buffers

    Args:
        filepath (str): The file path to the DSSF raw file
        indexes (list[int]): A list of indexes for lookup in the flatenned DSSF matrix
        ids (list[int]): A list of ids to associate results. Must have the same length of indexes
    Returns:
        a tuple with the datetime and a list of tuples (idx, value)
    """

    assert(len(indexes) == len(ids))

    newpath = tempfile.mktemp()

    # decompress the original file to a temporary file
    with open(newpath, 'wb') as new_file, bz2.BZ2File(filepath, 'rb') as file:
        for data in iter(lambda : file.read(100 * 1024), b''):
            new_file.write(data)

    # read and extract values from the temporary file
    date = None
    results = []
    with h5py.File(newpath, 'r') as h5f:
        # get the reference date of this data
        date = datetime.strptime(h5f['/'].attrs['SENSING_START_TIME'], '%Y%m%d%H%M%S')

        # read entire group as a numpy array and flatten it so we can access via vector indexes
        dssf = h5f['DSSF'][()].ravel()
        scaling_factor = h5f['DSSF'].attrs['SCALING_FACTOR']
        for i, idx in enumerate(indexes):
            results.append((ids[i], dssf[idx]/scaling_factor))
            
    # delete the temporary file 
    os.remove(newpath)

    return (date, results)
