import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sknn.mlp import Regressor, Layer

import utils
import regression

import sys
import logging

logging.basicConfig(
            format="%(message)s",
            level=logging.DEBUG,
            stream=sys.stdout)

data = utils.load_data()

# split into feature inputs and target
X = data.loc[:, data.columns[1]:data.columns[len(data.columns)-2]]
y = data.loc[:, 'radiation']

# scale features data
X = preprocessing.normalize(X)

# prepare train and test databases
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

nn = Regressor(
    layers=[
        Layer("Rectifier", units=14),
        Layer("Linear")],
    learning_rate=0.00001,
    n_stable=10,
    n_iter=200)

clf = GridSearchCV(nn, param_grid={
    'learning_rate': [0.000005, 0.00001, 0.000005, 0.000001],
    'hidden0__units': [14, 18, 22, 26, 30],
    'hidden0__type': ["Rectifier", "Sigmoid"]})
clf.fit(X, y)


print("Training")
clf.fit(X_train, y_train)

y_pred_train = clf.predict(X_train)
print("Training dataset")
regression.report(y_train, y_pred_train)
print("")

y_pred_test = clf.predict(X_test)
print("Test dataset")
regression.report(y_test, y_pred_test)
