# Resultados para Random Forest usando somente os dados de satélite

## 5 árvores
raining dataset
REGRESSION REPORT:
-------------------
R2: 0.878173238193
RMSE: 82.8372238958
Mean Absolute Error: 41.6087526353
Median Absolute Error: 4.49
Explained Variance Score: 0.87819379869

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.390283203311
RMSE: 189.064295526
Mean Absolute Error: 111.159713115
Median Absolute Error: 37.8
Explained Variance Score: 0.390974982945

## 10 árvores
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.901539561543
RMSE: 74.9836183985
Mean Absolute Error: 40.3078443429
Median Absolute Error: 10.37
Explained Variance Score: 0.901542418301

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.452068376756
RMSE: 176.42852419
Mean Absolute Error: 105.099553279
Median Absolute Error: 35.565
Explained Variance Score: 0.452262980737

## 50 árvores
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.925529023354
RMSE: 65.0631006653
Mean Absolute Error: 38.163328893
Median Absolute Error: 13.324
Explained Variance Score: 0.925542700597

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.491397999727
RMSE: 170.886309928
Mean Absolute Error: 101.225393776
Median Absolute Error: 33.989
Explained Variance Score: 0.491547857597


## 100 árvores
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.927820345497
RMSE: 63.4641587232
Mean Absolute Error: 37.5479300946
Median Absolute Error: 13.289
Explained Variance Score: 0.927838787068

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.509099840317
RMSE: 171.349310658
Mean Absolute Error: 105.118069631
Median Absolute Error: 41.0895
Explained Variance Score: 0.509107259501

## Para 200 árvores
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.930338419717
RMSE: 63.329385283
Mean Absolute Error: 38.3107161776
Median Absolute Error: 14.0795
Explained Variance Score: 0.930371717584

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.509383026992
RMSE: 165.232663086
Mean Absolute Error: 99.0836403765
Median Absolute Error: 32.4335
Explained Variance Score: 0.509926934558

## 1000 árvores
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.931935104436
RMSE: 61.7266405593
Mean Absolute Error: 37.1794280322
Median Absolute Error: 13.5893
Explained Variance Score: 0.931967986438

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.494303730006
RMSE: 173.355525942
Mean Absolute Error: 103.78998863
Median Absolute Error: 34.142
Explained Variance Score: 0.49435747589


## 200 árvores com altitude do sol
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.938136483176
RMSE: 59.4479141433
Mean Absolute Error: 34.7339711876
Median Absolute Error: 10.5825
Explained Variance Score: 0.938152355156

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.342363022002
RMSE: 218.611901354
Mean Absolute Error: 166.825984436
Median Absolute Error: 136.3365
Explained Variance Score: 0.342365661757

### 200 árvores com horas do dia
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.956259883137
RMSE: 50.1568025355
Mean Absolute Error: 26.5237049368
Median Absolute Error: 1.128
Explained Variance Score: 0.956260867397

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.679487195989
RMSE: 133.792944224
Mean Absolute Error: 69.8132467213
Median Absolute Error: 1.5775
Explained Variance Score: 0.67958904058

### 200 árvores utilizando o seno da altitude do sol
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.956999549865
RMSE: 49.0896891865
Mean Absolute Error: 25.9227327829
Median Absolute Error: 0.85375
Explained Variance Score: 0.95699985805

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.676707723871
RMSE: 138.417169693
Mean Absolute Error: 73.1079569672
Median Absolute Error: 1.9445
Explained Variance Score: 0.676759492974

### 200 árvores com temperatura
REGRESSION REPORT:
-------------------
R2: 0.976319538684
RMSE: 36.9021714499
Mean Absolute Error: 18.6795001757
Median Absolute Error: 0.904
Explained Variance Score: 0.976320248168

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.81161637693
RMSE: 102.627522401
Mean Absolute Error: 52.0440446904
Median Absolute Error: 3.26
Explained Variance Score: 0.811670360942


### 1000 árvores com temperatura
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.975526101166
RMSE: 37.3109609121
Mean Absolute Error: 18.9865480583
Median Absolute Error: 0.9771
Explained Variance Score: 0.975528725362

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.828163023347
RMSE: 99.2858467273
Mean Absolute Error: 49.6089103731
Median Absolute Error: 2.078
Explained Variance Score: 0.828198290898

### 2000 árvores com temperatura
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.976687099432
RMSE: 36.2489287017
Mean Absolute Error: 18.2921428308
Median Absolute Error: 0.82495
Explained Variance Score: 0.976689231587

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.814689440991
RMSE: 104.182318556
Mean Absolute Error: 53.4777728167
Median Absolute Error: 2.7423
Explained Variance Score: 0.814698599646

# 200 árvores com temperatura e umidade
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.982464906415
RMSE: 31.3189309611
Mean Absolute Error: 15.1863553857
Median Absolute Error: 0.6915
Explained Variance Score: 0.982466502873

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.858731168291
RMSE: 91.7119881531
Mean Absolute Error: 44.9285619106
Median Absolute Error: 2.2555
Explained Variance Score: 0.858731518033

# 1000 árvores com temperatura e umidade
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.982464906415
RMSE: 31.3189309611
Mean Absolute Error: 15.1863553857
Median Absolute Error: 0.6915
Explained Variance Score: 0.982466502873

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.858731168291
RMSE: 91.7119881531
Mean Absolute Error: 44.9285619106
Median Absolute Error: 2.2555
Explained Variance Score: 0.858731518033

# 2000 árvores com temperatura e umidade
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.982485532104
RMSE: 31.5057517382
Mean Absolute Error: 15.4316545247
Median Absolute Error: 0.692100000002
Explained Variance Score: 0.982486870415

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.861271456989
RMSE: 89.5765155921
Mean Absolute Error: 43.0360238417
Median Absolute Error: 2.2682
Explained Variance Score: 0.861299056105

# 200 arvores, temperatura + umidade, IR_120 - IR_087
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.981425527289
RMSE: 32.6312757634
Mean Absolute Error: 15.7225044808
Median Absolute Error: 0.8075
Explained Variance Score: 0.981426486637

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.871400881264
RMSE: 85.1115624945
Mean Absolute Error: 42.4191500615
Median Absolute Error: 2.986
Explained Variance Score: 0.871420162887

# 1000 arvores, temperatura + umidade, IR_120 - IR_087
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.981361034816
RMSE: 32.3781644848
Mean Absolute Error: 15.7772108241
Median Absolute Error: 0.848499999999
Explained Variance Score: 0.981361534066

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.87454237922
RMSE: 85.9204770124
Mean Absolute Error: 42.1465582616
Median Absolute Error: 2.059
Explained Variance Score: 0.874547820526

# 2000 arvores, temperatura + umidade, IR120 - IR087
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.98288176519
RMSE: 31.1917151673
Mean Absolute Error: 15.3404525215
Median Absolute Error: 0.96545
Explained Variance Score: 0.982882470724

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.859964642197
RMSE: 89.7131057214
Mean Absolute Error: 43.3176342558
Median Absolute Error: 2.1363
Explained Variance Score: 0.860310365173


# 200 arvores, temperatura + umidade, IR120 - IR087, temperature_delta
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.985145036476
RMSE: 29.0472298985
Mean Absolute Error: 14.0375053631
Median Absolute Error: 1.006
Explained Variance Score: 0.985146591597

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.891256289039
RMSE: 79.1740346249
Mean Absolute Error: 39.5390393765
Median Absolute Error: 2.341
Explained Variance Score: 0.891261319434

# 1000 arvores, temperatura + umidade, IR120-IR087, delta(temp)
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.985553677127
RMSE: 28.8820237265
Mean Absolute Error: 13.9663388606
Median Absolute Error: 1.0617
Explained Variance Score: 0.985556409945

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.897033064842
RMSE: 75.5472166041
Mean Absolute Error: 37.3212038966
Median Absolute Error: 2.19995
Explained Variance Score: 0.897072597465


# 1000 arvores, temperatura + umidade, IR120-IR087, delta(temp, rh)
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.985676634271
RMSE: 28.3485281785
Mean Absolute Error: 13.6838699138
Median Absolute Error: 0.9585
Explained Variance Score: 0.985679397978

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.891981029319
RMSE: 80.0018115193
Mean Absolute Error: 39.1463763741
Median Absolute Error: 2.5557
Explained Variance Score: 0.892012220206


# 1000 arvores, temperatura + umidade + pressao, IR120-IR087, delta(temp, rh)
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.985155134179
RMSE: 28.708968184
Mean Absolute Error: 14.0871995427
Median Absolute Error: 1.16125
Explained Variance Score: 0.985156981904

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.909395560262
RMSE: 74.1146604652
Mean Absolute Error: 36.2325091058
Median Absolute Error: 2.6485
Explained Variance Score: 0.90939562976

# 1000 arvores, temperatura + umidade + pressao, IR120-IR087, delta(temp)
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.986111107611
RMSE: 28.4153998217
Mean Absolute Error: 13.8259376363
Median Absolute Error: 1.099
Explained Variance Score: 0.986112469806

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.884512734122
RMSE: 79.3555184866
Mean Absolute Error: 38.607121411
Median Absolute Error: 2.4352
Explained Variance Score: 0.885041467867

# 1000 arvores, temperatura + umidade + pressao, IR120-IR087, delta(temp, rh, pressure)
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.985716470995
RMSE: 28.5115827697
Mean Absolute Error: 13.9930929124
Median Absolute Error: 1.1039
Explained Variance Score: 0.985719506948

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.9001381333
RMSE: 75.7164374779
Mean Absolute Error: 36.6501274518
Median Absolute Error: 2.335
Explained Variance Score: 0.900143358278

# 1000 arvores, temp+rh+pr+ws, IR120-IR087, delta
Training dataset
REGRESSION REPORT:
-------------------
R2: 0.986898815348
RMSE: 27.5109339701
Mean Absolute Error: 13.6251565951
Median Absolute Error: 1.11285
Explained Variance Score: 0.986904154604

Test dataset
REGRESSION REPORT:
-------------------
R2: 0.902552909977
RMSE: 73.4692125474
Mean Absolute Error: 35.9846617152
Median Absolute Error: 2.4577
Explained Variance Score: 0.902592040264
