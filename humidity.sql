copy (select horestacao as station_id, to_char(hordatahora, 'YYYY-MM-DD HH24:MI') as datetime, horleitura as rh
from horaria
where horestacao in (select estcodigo from telemetria.estacao where estorgao=1 and esttipo = 'M')
and horsensor=2
and horqualidade=0
and hordatahora >= :start
and hordatahora < :end
order by hordatahora) to STDOUT delimiter ',' csv header;
