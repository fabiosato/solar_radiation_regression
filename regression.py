from sklearn.metrics import *
from sklearn.grid_search import GridSearchCV

from time import clock
import math

def report(y_true, y_pred):
    print("REGRESSION REPORT: ")
    print("-------------------")
    print("R2: %s" % r2_score(y_true, y_pred))
    print("RMSE: %s" % math.sqrt(mean_squared_error(y_true, y_pred)))
    print("Mean Absolute Error: %s" % mean_absolute_error(y_true, y_pred))
    print("Median Absolute Error: %s" % median_absolute_error(y_true, y_pred))
    print("Explained Variance Score: %s" % explained_variance_score(y_true, y_pred))


def grid_search(X_train, X_test, y_train, y_test, base_regressor, params, cv):
    print("STARTING GRID SEARCH:")
    print("---------------------")
    clf = GridSearchCV(base_regressor, params, cv=cv, verbose=True)
    start = clock()
    clf.fit(X_train, y_train)
    end = clock()
    print()
    print("Finished training in %s seconds" % (end - start))
    print()
    print("Best parameters found: %s" % clf.best_params_)
    print()
    print("Scores of the individual regressors: ")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/- %0.03f) for %r" % (mean_score, scores.std() * 2, params))
    print

    y_pred = clf.predict(X_test)
    print()
    return clf
