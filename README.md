Installation

# requires libgdal1-dev. notice that we need to specify compatible gdal version
pip install gdal==1.10.0 --global-option=build_ext --global-option="-I/usr/include/gdal/" 

# requires libgeos++-dev
pip install https://github.com/matplotlib/basemap/archive/v1.0.7rel.tar.gz
