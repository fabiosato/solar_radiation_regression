import pandas as pd
import math

def load_data():
    # read sun altitude
    data = pd.read_csv('data/features/station/25264916_sun_altitude.csv')

    # add hour of day column
    data['hour'] = data['timestamp'].map(lambda t: int(t[11:13]))

    # read measurements and merge them
    data = data.merge(pd.read_csv('data/features/station/25264916_temperature.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_humidity.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_pressure.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_wind_speed.csv'), on='timestamp', how='left')

    # add temperature and humidity delta
    data['temperature_delta'] = data['temperature'] - data['temperature'].shift(1)
    data['humidity_delta'] = data['humidity'] - data['humidity'].shift(1)
    data['pressure_delta'] = data['pressure'] - data['pressure'].shift(1)
    data['wind_speed_delta'] = data['wind_speed'] - data['wind_speed'].shift(1)

    # read satellite features
    files = []
    for m in range (8, 13):
        files.append("data/features/satellite_new/25264916_%04d_%02d_satellite_features.csv" % (2015, m))
    features = pd.concat((pd.read_csv(f) for f in files))

    features['IR120-IR108'] = features['IR_120'] - features['IR_108']
    
    features['timestamp'] = features.pop('# timestamp')
    # merge them and drop rows with NaN
    data = data.merge(features, on='timestamp', how='left')
    data['VIS008-VIS006'] = data['VIS008'] - data['VIS006']

    # merge with solar radiation measurements
    data = data.merge(pd.read_csv('data/features/station/25264916_solar_radiation.csv', names=['timestamp', 'radiation']), on='timestamp', how='left')

    # drop NaN
    data = data.dropna()

    # replace sun_altitude with its sine - Didn't show improvements with RF
    # data['sun_altitude'] = data['sun_altitude'].map(lambda s: math.sin(math.radians(s)))

    # remove rows where radiation is <= 0. No improvements
    # data = data[(data['radiation'] > 0)]

    return data
