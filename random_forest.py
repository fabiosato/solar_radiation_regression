import pandas as pd
import math
from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import train_test_split

import regression
import utils

def load_data():
    # read sun altitude
    data = pd.read_csv('data/features/station/25264916_sun_altitude.csv')

    # add hour of day column
    data['hour'] = data['timestamp'].map(lambda t: int(t[11:13]))

    # read measurements and merge them
    data = data.merge(pd.read_csv('data/features/station/25264916_temperature.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_humidity.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_precipitation.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_pressure.csv'), on='timestamp', how='left')
    data = data.merge(pd.read_csv('data/features/station/25264916_wind_speed.csv'), on='timestamp', how='left')

    # add delta
    #data['temperature_delta'] = data['temperature'] - data['temperature'].shift(1)
    #data['humidity_delta'] = data['humidity'] - data['humidity'].shift(1)
    #data['pressure_delta'] = data['pressure'] - data['pressure'].shift(1)
    #data['wind_speed_delta'] = data['wind_speed'] - data['wind_speed'].shift(1)

    # read satellite features
    files = []
    for m in range (1, 13):
        files.append("data/features/satellite_new/25264916_%04d_%02d_satellite_features.csv" % (2015, m))
    features = pd.concat((pd.read_csv(f) for f in files))

    features['timestamp'] = features['# timestamp']
    features = features.drop('# timestamp', axis=1)
    #features['IR120-IR108'] = features['IR_120'] - features['IR_108']

    # merge them and drop rows with NaN
    data = data.merge(features, on='timestamp', how='left')
    print(data.columns)
    #data['VIS008-VIS006'] = data['VIS008'] - data['VIS006']

    # merge with solar radiation measurements
    data = data.merge(pd.read_csv('data/features/station/25264916_solar_radiation.csv', names=['timestamp', 'radiation']), on='timestamp', how='left')

    # drop NaN
    data = data.dropna()

    # replace sun_altitude with its sine - Didn't show improvements with RF
    # data['sun_altitude'] = data['sun_altitude'].map(lambda s: math.sin(math.radians(s)))

    # remove rows where radiation is <= 0. No improvements
    # data = data[(data['radiation'] > 0)]

    return data


# data = utils.load_data()
data = load_data()

# split into feature inputs and target
X = data.loc[:, data.columns[1]:data.columns[len(data.columns)-2]]
#print(X.head(10))
y = data.loc[:, 'radiation']
#print(y.head(10))

# prepare train and test databases
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

estimators = 200
clf = RandomForestRegressor(n_estimators=estimators, n_jobs=2)

clf.fit(X_train, y_train)

y_pred_train = clf.predict(X_train)
print("Training dataset")
regression.report(y_train, y_pred_train)
print("")

y_pred_test = clf.predict(X_test)
print("Test dataset")
regression.report(y_test, y_pred_test)
print("")

print("Entire dataset")
y_pred = clf.predict(X)
regression.report(y, y_pred)