#coding: utf-8

import csv
import numpy as np

codes = np.array(['code', 'IR_016', 'IR_039', 'IR_087', 'IR_097', 'IR_108', 'IR_120', 'IR_134', 'VIS006', 'VIS008', 'WV_062', 'WV_073'])

dict_error = {}

stations = []
with open('../../../data/features/station/stations.csv', 'r') as csvfile:
    for row in csvfile:
        stations.append(row.split(',')[0])
        
        
     
for id_station in stations[20:]:
    print '======================================================================='
    print id_station
    for month in ['08', '09', '10', '11', '12']:
        print '--------------------------------------------------------------------'
        print month
        filename = id_station+'_2015_'+month+'_satellite_features.csv'
        try:
            with open(filename, 'r') as csvfile:
                for row in csvfile:
                    data_row = np.array(row.split(','))
                    index = np.where(data_row=='')[0]
                    if len(index)>0:
                        #errors.append([data_row[0], codes[np.array(index)]])
                        print data_row[0], codes[np.array(index)]
            #dict_error[id_station][month] = errors
        except:
            print filename
            
            
'''
11
2015-11-16 00:00 ['IR_108']
2015-11-16 01:00 ['IR_108']
2015-11-20 18:00 ['IR_108']
2015-11-24 00:00 ['IR_108']
2015-11-24 01:00 ['IR_108']
2015-11-24 02:00 ['IR_108']
2015-11-24 03:00 ['IR_108']
2015-11-24 04:00 ['IR_108']
2015-11-24 05:00 ['IR_108']
2015-11-24 06:00 ['IR_108']
2015-11-24 07:00 ['IR_108']
2015-11-24 08:00 ['IR_108']
2015-11-24 09:00 ['IR_108']
2015-11-24 10:00 ['IR_108']

08
2015-08-18 01:00 ['IR_087' 'VIS008']

'''