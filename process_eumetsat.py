#!/usr/bin/python
import os
import re
import sys
from netCDF4 import Dataset
import csv
import math
import numpy as np
from osgeo import gdal, osr

# configuration
# satellite root data directory
eumetsat_l2_dir = '/simepar/satelite/eumetsat/met-10/HR-SEVIRI/L2'

# subfolders of each channel
channels = ['IR_016', 'IR_039', 'IR_087', 'IR_097', 'IR_108', 'IR_120', 'IR_134', 'VIS006', 'VIS008', 'WV_062', 'WV_073']

# years and months to be processed
years = [2013,2014,2015]
months = [1,2,3,4,5,6,7,8,9,10,11,12]

# output filename
output_filename = 'data/eumetsat_features.csv'

# dictionary that will hold the stations metadata
stations = {}

# load weather stations metadata (id, name, lat, lon, elevation) from csv file
with open('data/stations.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        record = {}
        record['elevation'] = float(row['elevation'])
        record['lon'] = float(row['lon'])
        record['lat'] = float(row['lat'])
        record['name'] = row['name']
        id = row['id']
        stations[id] = record

# open the output file
output = open(output_filename, 'w')

# dictionary that will hold the extracted features
features = {}

# loop over the list of channels, years and months
pattern = re.compile("[0-9]{12}")

for y in years:
    if y not in features:
        # initialize sub dict for months of the year
        features[y] = {}
    for m in months:
        if m not in features[y]:
            # initialize sub dict for stations
            features[y][m] = {}
        for station_id in stations:
            if station_id not in features[y][m]:
                # initialize sub dict for channels
                features[y][m][station_id] = {}
            for ch in channels:
                llon, llat, ulon, ulat, delta_lon, delta_lat = None, None, None, None, None, None
                print y, m, station_id, ch
                path = '%s/%s/%04d/%02d/' % (eumetsat_l2_dir, ch, y, m)
                if (os.path.exists(path)):
                    for dirName, subdirList, fileList in os.walk(path):
                        for fname in fileList:
                            if (pattern.match(fname)):
                                path = '%s/%s' % (dirName, fname)
                                year = int(fname[0:4])
                                month = int(fname[4:6])
                                day = int(fname[6:8])
                                hour = int(fname[8:10])
                                minute = int(fname[10:12])

                                #print '%s %s %s %s %s' % (year, month, day, hour, minute)
                                ds = gdal.Open(path, gdal.GA_ReadOnly)
                                gt = ds.GetGeoTransform()
                                band = ds.GetRasterBand(1)
                                #data = band.ReadAsArray(0, 0, ds.RasterXSize, ds.RasterYSize)

                                nc = Dataset(path)
                                data = np.array(nc.variables[ch][0])

                                xres = gt[1]
                                yres = gt[5]
                                xmin = gt[0] + xres * 0.5
                                xmax = gt[0] + (xres * ds.RasterXSize) - xres * 0.5
                                ymin = gt[3] + (yres * ds.RasterYSize) + yres * 0.5
                                ymax = gt[3] - yres * 0.5

                                #print "nx=%d ny=%d xres=%f yres=%f xmin=%f xmax=%f ymin=%f ymax=%f" % (ds.RasterXSize, ds.RasterYSize, xres, yres, xmin, xmax, ymin, ymax)

                                for id in stations:
                                    s = stations[id]
                                    x = round((s['lon'] - xmin) / xres, 0)
                                    y = round((s['lat'] - ymax) / yres, 0)
                                    output.write("%s,%04d-%02d-%02d %02d:%02d,%s,%f\n" % (id, year, month, day, hour, minute, ch, data[x][y]))
output.close()
